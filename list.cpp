#include <iostream>
#include "list.hpp"
#include "node.hpp"



using namespace std;

namespace animalfarm {


const bool SingleLinkedList ::empty() const
{
   if (head == nullptr)
      return true;
   else
      return false;


}
void SingleLinkedList :: push_front( Node* newNode )
{
   if(newNode == nullptr)
   {
      return;
   }
   else
   {
      newNode -> next = head;
      head = newNode;
      count ++;
   }



}



Node* SingleLinkedList ::pop_front()
{
   
   if(head == nullptr)
   {
      return nullptr;
   }


   
   else
   {
     Node* returnValue = head; 
     head = head -> next;
     count --;
     return returnValue;
   }
   

}



Node* SingleLinkedList ::get_first() const
{
   return head;
}



Node* SingleLinkedList ::get_next( const Node* currentNode ) const
{
Node* nextNode;
nextNode = currentNode -> next;
return nextNode;


}


unsigned int  SingleLinkedList ::size() const
{

return count;

}







}

