///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file aku.cpp
/// @version 1.0
///
/// Exports data about all aku fish
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////
#include <string>
#include <iostream>

#include "aku.hpp"

using namespace std;

namespace animalfarm {

Aku::Aku( int  newWeight, enum Color newColor, enum Gender newGender ) {
   gender = newGender;         /// Get from the constructor... not all aku  are the same gender (this is a has-a relationship)
   species = "Katsuwonus pelamis";    /// Hardcode this... all aku  are the same species (this is a is-a relationship)
   scaleColor = newColor;       /// A has-a relationship, so it comes through the constructor
   favoriteTemp = 75;       /// An is-a relationship, so it's safe to hardcode.
   weight = newWeight;             /// A has-a relationship. Is or is not native.
}


const string Aku::speak() {
   return string( "Bubble bubble" );
}


/// Print our Aku  first... then print whatever information Fish holds.
void Aku::printInfo() {
   cout << "Aku" << endl;
   cout << "   Weight = [" << weight << "]" << endl;
   Fish::printInfo();
}
}

