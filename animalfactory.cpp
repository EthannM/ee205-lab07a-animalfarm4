#include <iostream>
#include <string>


#include "animalfactory.hpp" 
#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"
#include "mammal.hpp"
#include "bird.hpp"
#include "fish.hpp"
using namespace std;

namespace animalfarm {

   
Animal* AnimalFactory::getRandomAnimal() {
Animal* newAnimal = NULL;
int i = Animal::getRandomInteger() % 6; // This gives you a number from 0 to 5
switch (i) {
   case 0: newAnimal = new Cat(Animal::getRandomName(), Animal::getRandomColor(),Animal::getRandomGender());break;
   case 1: newAnimal = new Dog   (Animal::getRandomName(),Animal::getRandomColor(),Animal::getRandomGender());break;
   case 2: newAnimal = new Nunu (Animal::getRandomBool(), RED,Animal:: getRandomGender());break;
   case 3: newAnimal = new Aku (Animal::getRandomWeight(20,42), SILVER, Animal::getRandomGender());break;
   case 4: newAnimal = new Palila (Animal::getRandomName(), YELLOW,Animal::getRandomGender());break;
   case 5: newAnimal = new Nene  (Animal::getRandomName(), BROWN,Animal::getRandomGender());break;
}

return newAnimal;

};




































}
