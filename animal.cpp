///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////
#include <cstdlib>
#include <iostream>
#include <string>
#include <ctime>
#include "animal.hpp"


using namespace std;

namespace animalfarm {


Animal::Animal(){


cout << ".";

}


Animal::~Animal(){


cout << "x";

}



   
void Animal::printInfo() {
	cout << "   Species = [" << species << "]" << endl;
	cout << "   Gender = [" << genderName( gender ) << "]" << endl;
}


string Animal::genderName (enum Gender gender) {
   switch (gender) {
      case MALE:    return string("Male"); break;
      case FEMALE:  return string("Female"); break;
      case UNKNOWN: return string("Unknown"); break;
   }

   return string("Really, really Unknown");
};
	

string Animal::colorName (enum Color color) {
 switch (color) {
      case BLACK:  return string("Black"); break;
      case WHITE:  return string("White"); break;
      case RED:    return string("Red"); break;
      case SILVER: return string("Silver"); break;
      case YELLOW: return string("Yellow"); break;
      case BROWN:  return string("Brown"); break;
   }

   return string("Unknown");
};






Gender Animal::getRandomGender(){
 int i = rand() % 3;
switch(i){
      case 0: return MALE; break;
      case 1: return FEMALE; break;
      case 2: return UNKNOWN; break;
      } 
   return UNKNOWN;
 };


Color Animal:: getRandomColor(){
   int i = rand() % 5;
switch(i){
      case 0: return BLACK; break;
      case 1: return WHITE; break;
      case 2: return RED;  break;
      case 3: return SILVER; break;
      case 4: return YELLOW; break;
      case 5: return BROWN; break;
      } 
   return BLACK;
  };

bool Animal:: getRandomBool(){
   return (rand() % 2);

};

float Animal::getRandomWeight( const float from, const float to ) {

float x = ((rand() % (int)(to - from) ) + (int)from);

return (float)x; 

};

int Animal::getRandomInteger(){


return rand();

};



string Animal::getRandomName(){
int i = rand() % 6;
switch(i){
      case 0: return string("Biofrost"); break;
      case 1: return string("Wildturtle"); break;
      case 2: return string("Bjergsen");  break;
      case 3: return string("Faker"); break;
      case 4: return string("Gosu"); break;
      case 5: return string("Tyler1"); break;
      }
   return string( "Unknown");




};

	
} // namespace animalfarm
